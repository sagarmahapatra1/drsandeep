# Hair Cosmetic


**Hair shampoos:** to remove sebum and environmental dirt without decreasing cosmetic appearance of hair
Shampoos are expected to improve the hair cosmetically, while being tailored to the needs of various hair types as well as age and individual habits.

**Hair conditioners:** to improve hair manageability, decreasing hair static electricity, add lustre and enhance styling ease
Hairstyling aids: to maintain hair in fashionable arrangement while improving the quality of hair fibres

**Hair dyes:** to lighten or darken hair colour while covering grey hair
Permanent hair waving/straightening : to reposition hair keratin disulfide bonfs in a new curly or straight position


