# Low Level Laser Therapy (LLLT)



The ability of lasers to induce hair growth was incidentally noted as early as 1967 when Mester et al used LLLT to treat cancer in mice and excessive hair growth happened to them.
Increased ATP production and modulation of reactive oxygen species (ROS) induced cell stimulation.

Hairmax Laser comb in 2007 got clearance from FDA for Male pattern baldness and 2011 for treatment of Female pattern hair loss.
655 nm wavelength shows improvement in intermediate Alopecias since effective photobiostimulation depends upon minimum number of hair present.
Moreover, combining LLLT with topical Minoxidil and oral finasteride may act synergistic to enhance hair regrowth.

Due to the known beneficial effect on wound healing, it is conceivable that LLLT as an adjunctive therapy in hair transplant surgery may also reduce postoperative shedding, reduce healing time and increased graft patency.

