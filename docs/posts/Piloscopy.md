---
title: Piloscopy
description: The brainchild of Dr Carlos K Wesley has been years in the making and is currently, being refined to achieve maximal efficacy. What is piloscopy? . In simple words, it is basically an inverted FUE procedure. Yes, as simple as that might sound, it really does perplex the mind as to how such a thing is even possible.
---

The brainchild of **Dr Carlos K Wesley** has been years in the making and is currently, being refined to achieve maximal efficacy. What is piloscopy? . In simple words, it is basically an inverted FUE procedure. Yes, as simple as that might sound, it really does perplex the mind as to how such a thing is even possible.

First, a port of entry is created over the scalp in the safe donor via a small surgical incision. Next,  an instrument is introduced into the incision with the purpose of dissecting out a working cavity within which the subdermal arm of the instrument is then used to harvest grafts controlled by the operator and visual data is conveyed across via a screen so that adjustments can be made as required. The grafts are then suctioned off via a saline filled tube into a storage vial.

So why all the hooplah regarding piloscopy you might ask?
According to its creator Dr Carlos K Wesley, it can combine certain benefits of FUE AND FUT:

**Donor area** – no trauma to skin surface at follicle extraction sites
Recepient area- more connective tissue around the follicular unit
The potential with this new currently experimental technique could revolutionise the hair transplant scene, similar to the arrival of FUE years ago.

And once it does, you can be sure Derma Solutions will be at the forefront, delivering the latest and best in hair transplantation options.

