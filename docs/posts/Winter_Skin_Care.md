---
title: Winter Skin Care
description: The advent of winter brings along with it many hazards for the unprepared soul. Along with worsening existing skin conditions such as psoriasis and  atopic dermatitis, to name a few,  it can even inconvenience people with normal skin. Here are a few tips to beat the winter chill and step up your skin game.
---

The advent of winter brings along with it many hazards for the unprepared soul. Along with worsening existing skin conditions such as psoriasis and  atopic dermatitis, to name a few,  it can even inconvenience people with normal skin. Here are a few tips to beat the winter chill and step up your skin game.

Wash your face without drying it out
It is imperative to use a hydrating cleanser to wash your face so as not to strip the skin of its all important oils and subsequently drying it out. 

**Moisturize moisturize moisturize**
When the mercury drops, along with the humidity, our skin desperately needs a moisturization boost. Most moisturisers if used regularly and as advised, should provide marked improvement during winter. If a moisturiser with more ‘oomph’ is required, one with ceramides and hyaluronic acid, though pricey, should serve your winter needs and plump up your skin.

**Sunscreen – everyday**
People erroneously believe that they don’t need sun protection on a cloudy day. Uv exposure still occurs and application of sunscreens and using photoprotective materials is advised, after all, uv rays are invisible to the human eye. The damage you prevent adds up over the years, so stay committed.

**Lip balm – your pocket companion**
Chapped lips are a frequent occurrence during winter. So try to stay ahead of it and moisturize them by applying a hydrating balm often. Avoid constantly licking your lips and matte lipsticks since they tend to worsen the dryness.

**Hands and feet – don’t forget to treat**
Don’t neglect your hands and feet. Moisturize them regularly as well and since the skin over your feet are thicker, petroleum jelly works better over this area as a moisturizer.

These are but a few general tips on skin care during the winter. If you still suffer from skin issues despite following the above mentioned recommendations, please consult a qualified Dermatologist at the earliest.

