module.exports = {
    title: 'Dr Sandeep',
  	description: 'Dermatologist',
    base: '/drsandeep/',
    dest: 'public',
    themeConfig : {
    logo: '/images/logo/logo_circle.png',
      nav : [
        { text : 'Profile', link : '/about/Dr_Sandeep_Mahapatra'},
        { text : 'Posts', link : '/posts/'},
      ],
      sidebar: {
      // '/recipes/pickle/': [
      //   '',
      //   'pickle',     /* /foo/ */
      //   'pickle-2',  /* /foo/one.html */
      //   'pickle-3',
      //   'pickle-4',
      //   'pickle-and-others',

      // ],

      '/posts/': [
        '',

       ],

       // '/references/': [
       //  'Resources_Natural_Building',
       //  'Resources_Permaculture',
       //  'Resources_Water_Conservation',
       //  'Inspirational_Stories',

       // ],
      // '/medicines/': [
      //   'chavyanprash',      /* /bar/ */
      //   'alovera', /* /bar/three.html */
      // ],

      // // fallback
      // '/poems/': [
      //   'halarda', /* /contact.html */
      // ]
     }
  }
}