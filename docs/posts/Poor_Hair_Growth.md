# Poor Hair Growth

Depends upon lot of factors

- Technique of graft desiccation in FUT
- Rough handling of graft
- Prolonged out of body time
- Sterile folliculitis which might be due to ingrown hairs, foreign body reaction or piggy backed graft insertion
- Dense placement of grafts more than 50-60 grafts/sq cm leading to ischemia and necrosis of recipient area
- Unsterile operating theatre conditions
- Implantation over already existing hairs resulting in shock loss
- Patient not taking proper post operative care
- Improper usage of post operative medicines which are essential for growth
- Grafts aren’t preserved properly

