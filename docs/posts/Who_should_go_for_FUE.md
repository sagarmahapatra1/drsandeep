# Who should go for FUE


FUE is best choice in following cases

-         Patient wants to have least painful post operative course
-         Patient scalp donor area is very inelastic
-         Patient wants to keep hair short ( buzz cut) which is very popular nowadays to avoid scar
-         Patient had donor area healing problem from previous strip or FUT surgery
-         Patient wants to resume his daily activities soon after the procedure like gymming etc
-         Where body hairs need to be extracted
-         Where finer hairs needs to be selected for hairline or eyebrow restoration
-         Repairing post strip or FUT donor scar area
-         Patient has a history of keloid or hypertrophic scarring
-         Patient has low donor density

