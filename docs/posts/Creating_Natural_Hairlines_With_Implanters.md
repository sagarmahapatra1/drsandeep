---
title: Creating Natural Hairlines With Implanters
description: Creating a natural hairline has always been one of the most important elements of a successful hair transplant. Our ability to create a natural hairline has dramatically increased over the years. Today, patients expect an *undetectable* hairline that has substantial density to stand on its own and wont tolerate an embarrassing pluggy or unnatural one.
---

# Creating Natural Hairlines With Implanters

Creating a natural hairline has always been one of the most important elements of a successful hair transplant. Our ability to create a natural hairline has dramatically increased over the years. Today, patients expect an *undetectable* hairline that has substantial density to stand on its own and wont tolerate an embarrassing pluggy or unnatural one.
We are better equipped now, than in the past, to create hairlines that meets these higher expectations. In part, this is simply due to the use of greater numbers of smaller, more natural looking follicular unit grafts (FU’s) in the hairline region. FU’s have given us *finer paintbrushes* with which to create a hairline. 


![](https://paper-attachments.dropbox.com/s_37098FB62907B6F78C3420C4D1419360C99517E528DE394998450416084A085F_1554116117550_image.png)


However the majority focus only on the most anterior border of the hairline, commonly referred to as the transition *zone*. In contrast, I find it useful to conceptualize the hairline as a larger, more extended region, approximately 2-3 cm deep, bridging the bald forehead to the area of central density. (Fig.-2) [8. 9]

This extended hairline region can than be divided into three smaller zones:

- The anterior portion which is the traditional *transition zone,*
- The posterior portion called the defined *zone.*
- A small oval area in the central portion of the defined zone called the *frontal tuft area.*

All three zones make their own unique contribution to the overall appearance of the hairline. ****

**Transition Zone** 
The transition zone consists of the first 0 .5-1 cm of the hairline. It should initially appear irregularly irregular and ill defined but gradually take on more definition and substance as it reaches the defined zone

**Micro-irregularity**– It is important to vary the density along the transition zone. Close examination of normal hairlines reveals that small, “intermittent”, triangular shaped areas of higher density contribute a great deal to the appearance of irregularity. 

**Macro-irregularity** – If one stands back and looks at a normal the hairline from a distance the path of the anterior border is seen to be more serpentine or curvaceous than linear. Only one-hair FU’s should be used in the anterior portion of the transition zone with a shift towards two-hair FU’s toward its posterior portion.

**Defined Zone** 
The defined zone is the 2-3 cm wide area that sits directly posterior to the transition zone. In this area the hairline should develop a higher degree of definition and density, yet still appear totally natural (undetectable) under close examination. Concentrating two and three- hair FU’s in this area nicely accomplishes both goals. **Frontal Tuft Area**
The frontal tuft is a small but aesthetically significant oval area that overlies the central portion of the defined zone directly behind the transition zone in the midline.
 Sufficient numbers of FU’s should be placed in the extended hairline region (transition zone + defined zone + frontal tuft) during the first session to ensure that it will be natural as well as have enough substance to stand on its own independent of further sessions. It is necessary to implant a density of approximately 20-30 FU’s/cm2. The extended hairline region usually measures about 30 cm2 in size if you include all three zones. So,  approximately 600-900 FU’s need to be placed in these zones during the first session to create this density (20-30 FU/cm2).
 
 **DIRECTION & PROPER ANGLE**
Angle and direction are distinct entities. Angle refers to the degree of elevation a hair as it exits the scalp. Direction refers to which way the hair points when leaving the scalp.
The hair along the frontal hairline is usually directed anteriorly and leaves the scalp at approximately a 10-15° angle. Existing miniaturized hairs can be used to find the proper angle and direction of hair in this area as well as occasionally assist in recreating cowlicks. 

 **LATERAL HUMP  AND THE FRONTO-TEMPORAL ANGLE** 
All-mature male hairlines have a fronto-temporal angle, which is formed by the junction of the frontal and temporal hairlines. Properly positioning this point and recreating a soft frontal temporal angle is one of the more difficult aspects of hairline recreation.

**IMPLANTERS**
**Implanter**s  are devices which allow the fitting of single, double, and triple follicular units. The advantages of this type of FUE device include decreased bleeding in the recipient site, reduced overall procedure times, and optimum survival rates of hair grafts thanks to the reduced trauma that hair grafts are exposed to during handling.
While creating hairlines we can manoevure the angle of the implanter, adjust the depth of the needle and choose different types of follicular units to be implanted at particular recipient site.
Surgeon’s expertise will allow to place the grafts very acutely while changing the angle of the implanters without making any pre slits. Precisely even optimum density can be achieved as we can calculate the required no of grafts per square cm as we usuallu use 0.8-0.9 mm implanter needles.
Creating irregularly irregular hairlines and lateral temple area merging is very much possible with the use of Implanters.

