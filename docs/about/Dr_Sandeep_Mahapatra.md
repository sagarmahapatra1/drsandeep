




<img width="100px" src="https://paper-attachments.dropbox.com/s_1C8148A3D49CE6C052CF7B1BC26363F680B2B52C11BBBFD48171BBD3351F4898_1554112969865_sandeep_circle.png"></img>
# Dr Sandeep Mahapatra ( MBBS, MD, DVL )
## Consultant Dermatologist and Hair Transplant Surgeon

### Medical Director Dermasolutions Skin and Hair Clinic Bangalore
### Neo Follicle Hair Transplant Centre, Kochi

Dr Sandeep Mahapatra is a pioneer in the hair restoration industry with more than **14 years of experience** to his credit and has performed over **3200 successful hair transplants** in India and abroad, a benchmark for any surgeon in the world and also developed his own unique technique of neo follicle transplantation which he has perfected over the years.

He has trained and worked with some of the renowned hair transplant surgeons in the world and later became a faculty and trainer to many aspiring hair transplant surgeons across the globe conducting regular workshops.
He is a member of the **International Society of Hair Restoration (ISHRS)**, **Indian Association of Hair Restoration Surgeons (IAHRS)**, **Association of Cutaneous Surgeons of India (ACSI) and Indian Association of Dermatology**, **Venerology and Leprosy (IADVL)**

He has been a faculty and course conductor at the World congress of Cosmetic Dermatology, Dermatologic and Aesthetic Surgeons International League (DASIL) and European Association of Dermatologists and Venerologists (EADV) with numerous presentations and publications to his credit.

Dr. Sandeep finished his MBBS from MGM MCH, Jamshedpur with Honours and is a gold medalist in two subjects and Post Graduation (MD) in Dermatology from RIMS, Ranchi.

He has earned his reputation as one of the best hair transplant surgeon in India. His areas of special interest include Hair Transplant, Beard Restoration, Eyebrow restoration and other skin and cosmetic surgeries.
Dr Sandeep has developed his own Neo Follicle Transplant technique after extensive research into many different methodologies of hair transplant. This technique has been shown to achieve the best possible hair graft survival which will enable a person to achieve the best results in terms of natural hair re growth.

His **Neo Follicle Hiar Transplant Clinic** is one of the most trusted and experienced hair restoration clinics in India with excellent results and positive patient feedback providing hair loss treatments and hair growth solutions for any type and stage of hair loss with clinics in **Bangalore**, **Cochin (Kochi/Ernakulum)**.

Dr Sandeep has performed more than 3200 procedures including those of celebrities like Top Film Stars, Sportsmen, Diplomats and Businessmen.
Dr Sandeep is also a Certified Cosmetologist in Advanced Botox and Dermal Fillers from Allergan, USA, Chemical Peels, Mesotherapy, Laser Hair Reduction, Dermaroller and Microdermabrasion.







