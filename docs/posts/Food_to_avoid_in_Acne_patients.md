---
title: Food to avoid in Acne patients
description: While the causative role of diet in acne hasn’t been proved yet, studies have definitely shown that it does influence the course of acne in patients, to varying degrees.
---

# Food to avoid in Acne patients


While the causative role of diet in acne hasn’t been proved yet, studies have definitely shown that it does influence the course of acne in patients, to varying degrees. The following recommendations have been strengthened via scientific studies:
Milk and dairy products
Studies have shown an association between milk/dairy products and acne. They tend to influence and bring about hormonal changes in the body, worsening acne in some patients.
High glycemic index food
Research has shown that foods belonging to the high glycemic index category can cause changes in insulin sensitivity and hormonal variations, all leading to worsening of acne. Foods to avoid include corn syrup, sugar, white flour(maida), refined grains, sauces, ketchup, sodas, sports drinks, juices, etc. Consume a diet rich in seasonal fruits and vegetables, seeds, nuts , whole grains, legumes and natural sources of healthy fats.
Omega 3 fatty acids
They are renowned for their anti inflammatory effect and help in the prevention and treatment of acne. Food rich in omega 3 fatty acids include – salmon, cod liver oil, oysters, soya bean, walnuts, chia seeds, flaxseeds etc
**Zinc** 
Zinc is a trace mineral and plays a considerable role in acne severity. Dietary sources of zinc include meat, poultry, fish and seafood as well as pumpkin seeds, cashews, quinoa, and lentils.
While there is no doubt that certain foods can help/worsen acne, we are far off from a food ‘cure’ for it. Before modifying your diet, it is important to talk to your doctor to make sure any changes you make don’t negatively affect your health.
Overall, the best diet advice in dealing with acne appears to be eating a wholesome, balanced diet rich in fresh fruits and vegetables, healthy and lean protein sources, and whole grains.

## DIET ESSENTIAL FOR HEALTHY HAIR

A healthy diet can strengthen your lustrous locks and help stave off some kinds of hair fall. If your diet is deficient in certain nutrients it could manifest as unhealthy hair and hair fall issues. Here are a few diet tips that will ensure your hair gets all the nutrients it deserves.

## PROTEIN

Hair is largely made of 'keratin’, which is a type of protein. A diet should be rich in protein to make sure the hair has enough building blocks while undergoing cyclical growth and shedding. Sources of lean protein are best and include fish, chicken, eggs, soy protein, nuts etc

1. ESSENTIAL FATTY ACIDS
2. They play a key role in healthy hair especially Omega 3 fatty acids. Sources of mega 3 fatty acids include- salmon, tuna, cod liver oil, flaxseeds, walnuts, almonds etc
3. Vitamin B7 and B12
4. Vitamins B7 (also known as biotin) and vitamin B12 are essential for healthy hair and nails. Vegetarian diets are usually lacking in vitamin B12 and need supplementation. Sources of biotin include- liver, egg took, nuts and seeds, avocado, dairy products. Sources of Vitamin B12 include- meat, poultry, fish and dairy products.
## TRACE MINERALS

Iron, magnesium and zinc are some of the trace minerals that help in hair growth and maintenance
While the right foods can do wonders for the hair, causes of hair loss can be numerous ranging from genetic , hormonal, autoimmune etc. Hence, it is crucial to visit a qualified dermatologist who can diagnose the root cause of hair loss and take appropriate action, the sooner the better.

