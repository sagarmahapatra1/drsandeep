# Platelet Growth Factors And hair Stem Cells

Growth factors derived from plasma for tissue repair and healing process of ulcers and wounds was already documented.
The action of growth factors on the germination Hair cycle has been studied in embryological and adult phases.
PRP has been used in several clinical studies for stimulating angiogenesis and cell proliferation and improve healing.
The growth factors contained in platelets of blood plasma are basically three:
**Platelet derived growth factor (PDGF)**
**Transforming growth factor (TGF)**
**Vascular Endothelial growth factor (VEGF)**
PRP induces proliferation of dermal papilla cells (DP) cells.
Protects DP cells from apoptosis
Increases cell growth and prolongs survival of hair follicles
PRP may prolong the Anagen phase of hair cycle.
Growth factors act in the bulge area, where stem cells are found and they interact with cells of matrix activating proliferative phase of hair.

